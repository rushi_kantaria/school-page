import Recact from 'react';
import maharidhi from "../assets/maharishi.png";

function LogoContainer() {

  const mainContainer = {
    backgroundColor: "white", 
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 30,
    paddingBottom: 30,
    flexDirection: "column"
  }

  return (
    <div>
        <div style={mainContainer}>
            <img src={maharidhi} style={{height: "50%", width: "70%"}} />
            <a style={{marginTop: 20}}>Maharidhi Vidhyamandir</a>
        </div>
    </div>
  );
}

export default LogoContainer;
