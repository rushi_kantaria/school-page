const DropDown = (props) => {

    const changeDataType = (event) => {
        props.handelPress(event.target.value);
    }

    return (
        <div style={{flexGrow: 1}}>
            <div style={{ border: '1px solid black', padding: 6, display: "flex", flexDirection: "row", marginTop: 20, ...props.style }}>
                <a style={{display: "flex", flex: 0.1}}>{props.label}</a>
                <select
                    style={{ borderWidth: 0, backgroundColor: "transparent", color: "red", marginLeft: 40, flex: 0.85 }}
                    onClick={changeDataType}
                >
                    {props.value.map((item) => <option value={item}>{item}</option>)}
                </select>
            </div>
        </div>
    );
}

export default DropDown;
