import Recact from 'react';
import maharidhi from "../assets/maharishi.png";

function CourseContainer(props) {

    const { data, selectedChapter, selectedTopic } = props;

    const mainContainer = {
        backgroundColor: "white",
        paddingLeft: 20,
        paddingRight: 20,
        display: "flex",
        flexDirection: "column"
    }

    const courseContainer = {
        height: 100,
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContaint: "center",
        alignItems: "center",
        border: '1px solid black',
        marginTop: 20
    }

    const subContainer = {
        height: 100,
        width: "100%",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        border: '1px solid black',
    }

    const textStyle = {
        marginLeft: 20,
        fontSize: 25
    }

    const shareContainer = {
        backgroundColor: "red",
        height: "100%",
        width: "5%"
    }

    return (
        <div>
            <div style={mainContainer}>
                {data.length>0 && data.map((item) => {
                    return(
                        <div>
                        {item ? <div style={courseContainer}>
                        <div style={subContainer}>
                            <img src={maharidhi} style={{ height: "100%", width: "14%" }} />
                            <a style={textStyle}>{`${item.chapter_name_original} (${item.video_title.split("|")[1].slice(5,11)})`}</a>
                        </div>
                        <div style={shareContainer}>

                        </div>
                    </div>:null}
                    </div>
                );
                })}
            </div>
        </div>
    );
}

export default CourseContainer;
