import React, {useEffect, useState} from 'react';
import LogoContainer from "./LogoContainer";
import DetailContainer from "./DetailContainer";
import DropDown from "./DropDown";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchData } from "../actions/getDataAction";
import CourseContainer from "./CourseContainer";

function Home(props) {

  const mainContainer = {
    backgroundColor: "black",
    paddingVertical: 50,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }

  const detailsContainer = {
    width: "90%",
    marginTop: 20,
    display: "flex",
    flexDirection: "column"
  }

  const upperContainer = {
    width: "100%",
    marginTop: 20,
    display: "flex",
    flexDirection: "row"
  }

  const buttonStyle = {
    marginLeft: "45%",
    backgroundColor: "black",
    color: "white",
    padding: 10,
    paddingLeft: 40,
    paddingRight: 40
  }

  const language = ["select any language","Hindi", "English"];
  const subject = ["select any subject","Maths"];
  const classes = ["select classes","1", "2"];

  const [selectedLenguage, setSelectedLenguage] = useState("Hindi");
  const [selectedSubject, setSelectedSubject] = useState("Maths");
  const [selectedClasses, setSelectedClasses] = useState("1");
  const [selectedChapter, setSelectedChapter] = useState(props?.schoolData[0]?.chapter_name_english);
  const [selectedTopic, setSelectedTopic] = useState(props?.schoolData[0]?.video_topic_name);
  const [data, setData] = useState([]);
  const [chapterdata, setChapterdata] = useState(undefined);
  const [topicData, setTopicData] = useState(undefined);
  const [error, setError] = useState(false);

  useEffect(() => {
    setSelectedChapter(chapterdata ? chapterdata : props?.schoolData[0]?.chapter_name_original);
    setSelectedTopic(topicData ? topicData : (selectedLenguage === "English" ? props?.schoolData[0]?.video_topic_name : props?.schoolData[0]?.video_title.split("|")[0]));
  })

  let chapterchapter = []
  {props.schoolData.length > 0 && props.schoolData.map((item) => chapterchapter.push(item.chapter_name_original))}
  let Chapter = chapterchapter.length > 0 && chapterchapter.filter((item, index) => {return chapterchapter.indexOf(item) === index});

  let topicTopic = []
  {props.schoolData.length > 0 &&  props.schoolData.map((item) => topicTopic.push(selectedLenguage !== "English" ? item.video_title.split("|")[0] : item.video_topic_name))}
  let topic = topicTopic.length > 0 && topicTopic.filter((item, index) => {return topicTopic.indexOf(item) === index});


  const handelSubmit = async () => {
    let sectedChapterinenglish = props.schoolData.find((item) => item.chapter_name_original === selectedChapter ? item.chapter_name_english : null);
    let sectedTopicINEnglish = props.schoolData.find((item) => item.video_title.split("|")[0] === selectedTopic ? item.video_topic_name : null);
    let updatedData = props.schoolData.map((item) => {
      if(selectedLenguage !== "English" && sectedChapterinenglish.chapter_name_original.toUpperCase() === item.chapter_name_original.toUpperCase() && 
          sectedTopicINEnglish.video_topic_name.toUpperCase() === item.video_topic_name.toUpperCase()){
          return ( item )
      }else if (selectedChapter.toUpperCase() === item.chapter_name_english.toUpperCase() && 
      selectedTopic.toUpperCase() === item.video_topic_name.toUpperCase()){
        return ( item )
      }
    }) 
    setData(updatedData);
  }

  const fetchdatalaguage = async(value) => {
    await props.fetchData(selectedClasses, value);
    setSelectedLenguage(value);
  }

  const fetchclasses = async(value) => {
    console.log(value);
      value === "select classes" ? 
      setError(true) :
      await props.fetchData(value, selectedLenguage);
      setSelectedClasses(value);
      setError(false)
  } 

  return (
    <div style={{ display: "flex", flexDirection: "column", alignItems: "center", backgroundColor: "#ECECEC", paddingBottom: 20 }}>
      <div style={mainContainer}>
        <h1 style={{ color: "white" }}>TACL SAAN</h1>
      </div>
      <div style={detailsContainer}>
        <div style={upperContainer}>
        <div style={{flex: 0.2}}>
          <LogoContainer />
        </div>
          <div style={{ flex: 0.9, marginLeft: 20 }}>
            <div style={{ bottom: 0, backgroundColor: "white", height: "80%", padding: 20 }}>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <div style={{ display: "flex", backgroundColor: "white", marginLeft: 20, flexDirection: "column", flexGrow: 0.9 }}>
                  <DropDown 
                    value={language} 
                    label={"language"} 
                    handelPress={(value) => fetchdatalaguage(value)}
                  />
                  <DropDown 
                    value={Chapter ? Chapter : ["no data found"]} 
                    label={"Chapter"} 
                    handelPress={(value) => setChapterdata(value)}
                  />
                </div>
                <div style={{ display: "flex", backgroundColor: "white", marginLeft: 20, flexGrow: 0.7, flexDirection: "column" }}>
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <DropDown 
                      value={subject} 
                      label={"subject"} 
                      handelPress={(value) => setSelectedSubject(value)}
                    />
                    <div style={{flexDirection: "column", display: "flex"}}>
                    <DropDown 
                      value={classes} 
                      label={"classes"} 
                      style={{ marginLeft: 20 }} 
                      handelPress={(value) => fetchclasses(value)}
                    />
                    {error ? <a style={{color: "red"}}>please select the correct class</a> : null}
                    </div>
                  </div>
                  <DropDown 
                    value={ topic ? topic : ["no data found"]} 
                    label={"Topic"} 
                    handelPress={(value) => setTopicData(value)}
                  />
                </div>
              </div>
              <div style={{ display: "flex", marginTop: 30 }}>
                <a style={buttonStyle} href="#" onClick={() => handelSubmit()}>Go</a>
              </div>
            </div>
          </div>
          </div>
          <div style={{display: "flex", flexDirection: "row"}}>
            <div style={{flex: 0.2}}>
              <DetailContainer />
            </div>
            <div style={{marginTop: 20, flex: 0.9, marginLeft: 20}}>
             {selectedLenguage.length > 0 ?  <CourseContainer 
                data={data ? data : null}
                selectedSubject={selectedSubject}
                selectedChapter={selectedChapter}
                selectedTopic={selectedTopic}
             /> : <a style={{color: "red"}}>please select all detail</a>}      
        </div> 
          </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  schoolData: state.getData.schoolData,
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        fetchData
      },
      dispatch
    ),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
