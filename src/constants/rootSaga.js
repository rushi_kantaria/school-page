import {all} from "redux-saga/effects";
import { waitForFetchData } from "./sagas/getDataSaga";

export default function* rootSaga() {
    yield all([waitForFetchData()])
}