import * as actionType from "../actionType";

const initialState = {
    schoolData: {},
  };
  
  export default (state = initialState, action) => {
    switch (action.type) {
      case actionType.ADD_DATA:
        return {
          ...state,
          schoolData: action.value,
        };   
      default:
        return state;
    }
  };
  