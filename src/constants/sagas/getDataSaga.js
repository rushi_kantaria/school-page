import { takeEvery, call, put } from "redux-saga/effects";
import * as actionType from "../actionType";

function* fetchData(action) {
    try{
        console.log("before fetch");
        const json = yield fetch(`http://143.110.242.59:8081/api/video/?language=${action.selectedLenguage}&page=${action.selectedClasses}`)
        .then(response => response.json(), );
        yield put({type: actionType.ADD_DATA, value: json.results});
    }catch (e) {}
}

export function* waitForFetchData() {
    console.log("watch")
    yield takeEvery(actionType.FETCH_DATA, fetchData);
}